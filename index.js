'use strict';

const express = require('express');

const server = express();

server.get('/', (request, response) => {
  response.sendFile('headers.html', {root: 'public'});
});

server.get('/api/whoami', (request, response) => {
  const output = {
    ip: request.ip,
    agent: request.headers['user-agent'],
    language: request.headers['accept-language']
  };
  if (request.headers['x-forwarded-for']) {
    output.ip = request.headers['x-forwarded-for'].split(',')[0];
  }
  response.json(output);
});

server.listen(8080);
